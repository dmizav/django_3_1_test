from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.utils import timezone
from collections import OrderedDict
from .models import Poll, Question, Choice, UserChoice, User


class IndexView(generic.ListView):
    """Главная страница"""
    template_name = 'polls/index.html'
    context_object_name = 'poll_list'
    queryset = Poll.objects.filter(is_pub=True).order_by('-pub_date')


@method_decorator(login_required(login_url='polls:login'), name='dispatch')
class QuestionView(generic.TemplateView):
    """Страница опроса"""
    model = Question
    template_name = 'polls/question.html'

    def dispatch(self, request, *args, **kwargs):
        poll_is_not_pub = Poll.objects.filter(is_pub=False, id=get_pk(self)).exists()
        user_is_staff = User.objects.filter(username=request.user.username,
                                                is_staff=True).exists()
        user_answered = UserChoice.objects.filter(user__username=request.user.username,
                                                 poll_id=get_pk(self),
                                                 ).exists()
        if poll_is_not_pub:
            """Если опрос не опубликован, не даем перейти по его url"""
            return HttpResponseRedirect(reverse('polls:index', ))
        elif user_is_staff:
            """Администраторы только СОЗДАЮТ опросы, запрос логина"""
            return HttpResponseRedirect(reverse('polls:login', ))
        elif user_answered:
            """Если уже проходили опрос, то смотри результаты"""
            return HttpResponseRedirect(reverse('polls:results', args=(get_pk(self),)))
        else:
            """Наконец, можем пройти опрос"""
            return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return make_question_context(get_pk(self), context)


@method_decorator(login_required(login_url='polls:login'), name='dispatch')
class ResultsView(generic.TemplateView):
    """Страница результатов"""
    model = Question
    template_name = 'polls/results.html'

    def dispatch(self, request, *args, **kwargs):
        poll_is_pub = Poll.objects.filter(is_pub=True, id=get_pk(self)).exists()
        user_answered = UserChoice.objects.filter(user__username=request.user.username,
                                                 poll_id=get_pk(self),
                                                 ).exists()
        if poll_is_pub and user_answered:
            """Могут смотреть результаты только ответившие и только опубликованные опросы"""
            return super().dispatch(request, *args, **kwargs)
        else:
            """В противном случае не пускаем"""
            return HttpResponseRedirect(reverse('polls:index', ))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context = make_question_context(get_pk(self), context)
        question_dic = context['question_dic']
        choice_dic = context['choice_dic']
        for question in question_dic:
            sum_votes = sum([choice_dic[choice] for choice in question_dic[question]])
            for choice in question_dic[question]:
                """Добавил новый votes в choice. Не знаю, на сколько это хорошо. Но так наиболее удобно."""
                choice.votes = round(choice_dic[choice]*100/sum_votes, 2)
        return context


def get_pk(class_self):
    """Получаем номер опроса (страницы) из аргументов"""
    return class_self.kwargs.get('pk')


def make_dics(poll_id):
    """question_dic = Ordered {question_obj:[choice_obj, choice_bj, ..]} для нужного poll_id
       choice_dic = {choice_obj:sum_votes} для нужного poll_id
    """
    question_dic = OrderedDict()
    choice_dic = {}
    for question in Question.objects.filter(poll_id=poll_id).order_by('position'):
        choices = Choice.objects.filter(question_id=question.id).order_by('position')
        question_dic[question] = choices
        for choice in choices:
            choice_dic[choice] = UserChoice.objects.filter(choice_id=choice.id).count()
    return question_dic, choice_dic


def make_question_context(poll_id, context):
    """Собираем context"""
    context['question_dic'], context['choice_dic'] = make_dics(poll_id)
    context['poll'] = Poll.objects.get(pk=poll_id)
    return context


def vote(request, poll_id):
    questions_in_poll = [str(x['id']) for x in Question.objects.filter(poll_id=poll_id).values('id')]
    if set(questions_in_poll) - set([str(x) for x in request.POST.keys()]) == set():
        """Убеждаемся, что ответили на все вопросы в опросе"""
        for question_id in questions_in_poll:
            choice_id = request.POST[question_id]
            choice = Choice.objects.get(id=choice_id)
            user_choice = UserChoice(user = User.objects.get(username=request.user.username),
                                    poll = Poll.objects.get(id=poll_id),
                                    question = Question.objects.get(id=question_id),
                                    choice = choice,
                                    vote_date = timezone.now()
                                     )
            user_choice.save()
        return HttpResponseRedirect(reverse('polls:results', args=(poll_id,)))
    else:
        """Если ответили не на все вопросы"""
        context = make_question_context(poll_id, {})
        context['pk'] = poll_id
        context['error_message'] = "Необходимо ответить на все вопросы"
        return render(request, 'polls/question.html', context)




