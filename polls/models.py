from django.db import models
from django.contrib.auth.models import User


class Poll(models.Model):
    poll_text = models.CharField('Название опроса', max_length=200)
    pub_date = models.DateTimeField('Дата публикации')
    is_pub = models.BooleanField('Опубликован', default=True)
    def __str__(self):
        return self.poll_text

    class Meta:
        verbose_name = 'Опрос'
        verbose_name_plural = 'Опросы'


class Question(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    question_text = models.CharField('Вопрос', max_length=200)
    position = models.PositiveSmallIntegerField('Порядок', null=True)

    def __str__(self):
        return self.question_text

    class Meta:
        ordering = ['position']
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField('Ответ', max_length=200)
    position = models.PositiveSmallIntegerField('Порядок', null=True)

    def __str__(self):
        return self.choice_text

    class Meta:
        ordering = ['position']
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'


class UserChoice(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)
    vote_date = models.DateTimeField('Дата')

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'
