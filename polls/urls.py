from django.urls import path, re_path, include

from . import views
from django.contrib.auth.views import LoginView, LogoutView

app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.QuestionView.as_view(), name='question'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:poll_id>/vote/', views.vote, name='vote'),
    path('logged_out/', LogoutView.as_view(), name='logged_out'),
    path('login/', LoginView.as_view(), name='login'),
]