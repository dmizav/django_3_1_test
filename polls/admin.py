from django.contrib import admin
import nested_admin

from .models import Poll, Question, Choice, UserChoice, User


class ChoiseInline(nested_admin.NestedTabularInline):
    model = Choice
    classes = ['collapse']
    sortable_field_name = "position"
    min_num = 2
    extra = 0


class QuestionInline(nested_admin.NestedTabularInline):
    model = Question
    inlines = [ChoiseInline]
    classes = ['collapse']
    sortable_field_name = "position"
    min_num = 1
    extra = 0


class CategoryAdmin(nested_admin.NestedModelAdmin):
    list_display = ['poll_text', 'pub_date']
    ordering = ['-pub_date']
    inlines = [QuestionInline]
    classes = ['collapse']
    extra = 0


admin.site.register(Poll, CategoryAdmin)


class UserAdmin(admin.ModelAdmin):
    list_display =['user', 'poll', 'question', 'choice', 'vote_date']
    list_filter = ['user', 'poll']
    ordering=['-vote_date']


admin.site.register(UserChoice, UserAdmin)