# Generated by Django 2.1.3 on 2018-12-04 19:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0014_auto_20181204_2155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='poll',
            name='is_pub',
            field=models.BooleanField(default=True, verbose_name='Опубликован'),
        ),
        migrations.AlterField(
            model_name='poll',
            name='poll_text',
            field=models.CharField(max_length=200, verbose_name='Название опроса'),
        ),
    ]
