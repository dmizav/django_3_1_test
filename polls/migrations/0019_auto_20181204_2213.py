# Generated by Django 2.1.3 on 2018-12-04 19:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0018_auto_20181204_2212'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='position',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='Порядок'),
        ),
    ]
